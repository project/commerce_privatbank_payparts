Commerce privatbank payparts
===============

CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Author
  * Similar projects and how they are different

INTRODUCTION
------------
This module provides a Drupal Commerce payment method to embed the payment
payparts services provided by Privatbank

It efficiently integrates payments from various sources such as: