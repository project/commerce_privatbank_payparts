<?php

namespace Drupal\commerce_privatbank_payparts\PluginForm\OffsiteRedirect;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_payment\PluginForm\PaymentGatewayFormBase;
use Drupal\commerce_privatbank_payparts\Form\PrivatbankPaypartsFormTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides the class for payment off-site form.
 *
 * Provide a buildConfigurationForm() method which calls buildRedirectForm()
 * with the right parameters.
 */
class PrivatbankPaypartsPaymentForm extends PaymentGatewayFormBase {
  use PrivatbankPaypartsFormTrait;

  /**
   * Gateway plugin.
   *
   * @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface
   */
  private $paymentGatewayPlugin;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $this->paymentGatewayPlugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $this->paymentGatewayPlugin->getConfiguration();

    $parts_choices = [];
    foreach (range(2, 25) as $i) {
      $parts_choices[$i] = $i;
    }
    $form['partsCount'] = [
      '#type' => 'select',
      '#title' => $this->t('Parts count'),
      '#options' => $parts_choices,
      '#default_value' => $configuration['partsCount'],
      '#required' => True,
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Go to payment system'),
      '#submit' => [$this, 'submitConfigurationForm'],
      '#attributes' => ['class' => ['btn-primary']]
    ];
    return $form;
  }

  public function submitConfigurationForm(array &$form,  FormStateInterface $form_state) {
    $payment = $this->entity;
    $this->paymentGatewayPlugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $this->paymentGatewayPlugin->getConfiguration();
    $this->config = $configuration;
    $order = $payment->getOrder();
    $total_price = $order->getTotalPrice();
    $amount = number_format($total_price->getNumber(), '2', '.', '');

    $products = [];
    foreach ($order->getItems() as $i) {
      $products[] = ['name' => $i->getTitle(),
                     'count' => (int)$i->getQuantity(),
                     'price' => number_format($i->getTotalPrice()->getNumber(),
                       2, '.','')];
    }
    $merchant_data['storeId'] = $configuration['storeId'];
    $uuid = \Drupal::service('uuid');
    $uuid = $uuid->generate();
    $order_id = "{$payment->getOrderId()}-{$uuid}";
    $merchant_data['orderId'] = $order_id;
    $merchant_data['amount'] = $amount;
    $form_values = $form_state->getValue($form['#parents']);
    $merchant_data['partsCount'] = $form_values['partsCount'];
    $merchant_data['merchantType'] = $configuration['merchantType'];
    $merchant_data['products'] = $products;
    $merchant_data['responseUrl'] = Url::fromRoute('commerce_payment.notify',
      ['commerce_payment_gateway' => $payment->getPaymentGatewayId()],
      ['absolute' => TRUE])->toString();
    $merchant_data['redirectUrl'] = $form['#return_url'].'?privatbank_payparts_order_id='.$order_id;
    $merchant_data['signature'] = $this->makeSignature($merchant_data);
    $this->validatePaymentForm($merchant_data);
    $res = $this->api('https://payparts2.privatbank.ua/ipp/v2/payment/create', $merchant_data);
    $redirect_url = 'https://payparts2.privatbank.ua/ipp/v2/payment?token='.$res['token'];
    throw new NeedsRedirectException($redirect_url);
  }

}
