<?php

namespace Drupal\commerce_privatbank_payparts\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_privatbank_payparts\Form\PrivatbankPaypartsFormTrait;
use Drupal\commerce_privatbank_payparts\Form\PrivatbankPaypartsResponseForm;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Query\Truncate;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Off-site Redirect payment Privatbank payparts gateway.
 *
 * @CommercePaymentGateway(
 *   id = "privatbank_payparts",
 *   label = "Privatbank payparts (Off-site redirect)",
 *   display_label = "Privatbank payparts",
 *   forms = {
 *     "offsite-payment" =
 *   "Drupal\commerce_privatbank_payparts\PluginForm\OffsiteRedirect\PrivatbankPaypartsPaymentForm",
 *   },
 *   modes = {"live"},
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 * )
 */
class PrivatbankPayparts extends OffsitePaymentGatewayBase {
  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  public $logger;

  /**
   * Constructs a new PrivatbankPayparts object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    LoggerChannelFactoryInterface $logger
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition,
      $entity_type_manager, $payment_type_manager, $payment_method_type_manager,
      $time);
    $this->logger = $logger->get('commerce_privatbank_payparts');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $domain = \Drupal::request()->getHost();
    $defaults = [
      'storeId' => '',
      'password' => '',
      'partsCount' => 2,
      'merchantType' =>  'II',
    ];
    return $defaults + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
      array $form,
      FormStateInterface $form_state
  ) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['storeId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant store id'),
      '#default_value' => $this->configuration['storeId'],
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant password'),
      '#default_value' => $this->configuration['password'],
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $parts_choices = [];
    foreach (range(2, 25) as $i) {
      $parts_choices[$i] = $i;
    }
    $form['partsCount'] = [
      '#type' => 'select',
      '#title' => $this->t('Parts count'),
      '#options' => $parts_choices,
      '#default_value' => $this->configuration['partsCount'],
      '#required' => True,
    ];
    $merchant_type_choices = [
      'II' => $this->t('Instant installments'),
      'PP' => $this->t('Payment in parts'),
      'PB' => $this->t('Payment in parts. Money in the period.'),
      'IA' => $this->t('Instant installments. Promotional.'),
    ];
    $form['merchantType'] = [
      '#type' => 'select',
      '#title' => $this->t('Merchant type'),
      '#options' => $merchant_type_choices,
      '#default_value' => $this->configuration['merchantType'],
      '#required' => True,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
      array &$form,
      FormStateInterface $form_state
  ) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration = array_merge($this->configuration, $values);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $payment_storage = \Drupal::service('entity_type.manager')
                              ->getStorage('commerce_payment');
    /* @var $payment_storage \Drupal\commerce_payment\PaymentStorage */
    $payment_is_succeed = false;
    $config = $this->getConfiguration();
    $form = new PrivatbankPaypartsResponseForm([], $config);
    $order_id = !empty($_GET['privatbank_payparts_order_id'])?$_GET['privatbank_payparts_order_id']:$order->id();
    $request_data = [
      'storeId' => $config['storeId'],
      'orderId' => $order_id
    ];
    $request_data['signature'] = $form->makeSignature($request_data);
    $resp = $form->api('https://payparts2.privatbank.ua/ipp/v2/payment/state', $request_data);
    $payment_is_succeed = in_array($resp['paymentState'], [
      'SUCCESS',
    ]);
    if(!$payment_is_succeed) {
      throw new PaymentGatewayException($this->t('Error during payment %message %payment_state'), [
        '%message' => $resp['message'],
        '%payment_state' => $resp['payment_state'],
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    $config = $this->getConfiguration();
    $body = file_get_contents('php://input');
    $logger = $this->logger;
    $logger->debug($body);
    $post_data = json_decode($body, TRUE);
    if (is_null($post_data)) {
      $post_data = $_POST;
    }
    $response_code = 200;

    $form = new PrivatbankPaypartsResponseForm($post_data, $config);
    $form->payment_gateway = $this->entityId;
    if ($form->isValid()) {
      $form->save();
      $resp_data = 'ok';
    }
    else {
      $resp_data = $form->errors;
      $response_code = 400;
    }
    $resp_data_string = json_encode($resp_data);
    $logger->debug('Response: ' . $resp_data_string);
    return new JsonResponse($resp_data, $response_code);
  }

  /**
   * Currency default validation.
   *
   * @param string $transaction_currency
   *   Currency from Privatbank.
   * @param string $order_currency
   *   Currency from order.
   *
   * @return bool
   *   TRUE on successful validation FALSE otherwise.
   */
  protected function validateCurrency($transaction_currency, $order_currency) {
    if ($transaction_currency != $order_currency) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Amount default validation.
   *
   * @param float $transaction_amount
   *   Amount from Privatbank.
   * @param float $order_amount
   *   Amount from order.
   *
   * @return bool
   *   TRUE on successful validation FALSE otherwise.
   */
  protected function validateAmount($transaction_amount, $order_amount) {
    if ($transaction_amount != $order_amount) {
      return FALSE;
    }
    return TRUE;
  }

}
