<?php
namespace Drupal\commerce_privatbank_payparts\Form;

use Drupal\commerce_privatbank_payparts\Helpers\Arr;
use Drupal\commerce_privatbank_payparts\Helpers\Validation;

trait PrivatbankPaypartsFormTrait {
  /**
   * Gateway configuration.
   *
   * @var array
   */
  private $config;
  private static function withoutFloatingPoint($number) {
    if(is_null($number)) {
      return $number;
    }
    return number_format($number, 2, '', '');
  }
  private static function getProductString($products) {
    $product_arr = [];
    if(!$products) {
      return '';
    }
    foreach ($products as $product) {
      $product_arr[] = $product['name'];
      $product_arr[] = $product['count'];
      $product_arr[] = self::withoutFloatingPoint($product['price']);
    }
    $product_string = implode('', $product_arr);
    return $product_string;
  }
  /**
   * Returns Signature.
   *
   * @param array $cleaned_data
   *   Cleaned data.
   *
   * @return string
   *   Signature.
   */
  public function makeSignature(array $cleaned_data) {
    $data      = [
      Arr::get($cleaned_data, 'storeId', NULL),
      Arr::get($cleaned_data, 'orderId', NULL),
      Arr::get($cleaned_data, 'paymentState', NULL),
      Arr::get($cleaned_data, 'message', NULL),
      self::withoutFloatingPoint(Arr::get($cleaned_data, 'amount', NULL)),
      Arr::get($cleaned_data, 'partsCount', NULL),
      Arr::get($cleaned_data, 'merchantType', NULL),
      Arr::get($cleaned_data, 'responseUrl', NULL),
      Arr::get($cleaned_data, 'redirectUrl', NULL),
      self::getProductString(Arr::get($cleaned_data, 'products', NULL)),
    ];
    $sign_data = [];
    foreach ($data as $i) {
      if (is_null($i)) {
        continue;
      }
      elseif (is_array($i)) {
        foreach ($i as $i2) {
          $sign_data[] = (string) $i2;
        }
      }
      else {
        $sign_data[] = (string) $i;
      }
    }

    $sign_data_string = $this->config['password'].implode('', $sign_data).$this->config['password'];
    return base64_encode(sha1($sign_data_string, true));
  }

  /**
   * Api call.
   *
   * @param string $method
   *   Method name.
   * @param array $data
   *   Param.
   *
   * @return array
   *   Result.
   *
   * @throws \Exception
   */
  public function api($method, array $data) {
    /** @var \Drupal\Core\Logger\LoggerChannelInterface $logger */
    $logger       = \Drupal::logger('commerce_privatbank_payparts');
    $request_data = [];
    foreach ($data as $k => $v) {
      if ($v) {
        $request_data[$k] = $v;
      }
    }
    $request_data_string         = json_encode($request_data);
    $headers                     = [
      'Accept' => 'application/json',
      'Accept-Encoding' => 'UTF-8',
      'Content-Type'   => 'application/json',
      'Content-Length' => strlen($request_data_string),
    ];
    $options[CURLOPT_HTTPHEADER] = [];
    foreach ($headers as $key => $value) {
      $options[CURLOPT_HTTPHEADER][] = $key . ': ' . $value;
    }
    $options[CURLOPT_SSL_VERIFYPEER] = TRUE;
    $options[CURLOPT_SSL_VERIFYHOST] = 2;
    $options[CURLOPT_CUSTOMREQUEST]  = 'POST';
    $options[CURLOPT_POSTFIELDS]     = $request_data_string;
    $options[CURLOPT_RETURNTRANSFER] = TRUE;
    $curl                            = curl_init($method);
    curl_setopt_array($curl, $options);
    $body = curl_exec($curl);
    $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    // Close the connection.
    curl_close($curl);
    $logger->debug($body);
    $resp_json = json_decode($body, TRUE);
    if (is_null($resp_json)) {
      $error_code = 'unknown';
      $mss        = strtr('Error method {0} error: {1} code: {2} http_status: {3}',
        [
          '{0}' => $method,
          '{1}' => $body,
          '{2}' => $error_code,
          '{3}' => $code,
        ]);
      $logger->error($mss);
      throw new \Exception($mss);
    }
    $error_message = '';
    $error_code    = NULL;
    if (isset($resp_json['state']) == FALSE) {
      $error_message = $body;
      $error_code    = 'unknown';
    }
    elseif ($resp_json['state'] !== 'SUCCESS') {
      $error_message = $resp_json['message'];
      $error_code    = $resp_json['state'];
    }
    if (is_null($error_code) === FALSE) {
      $mss = strtr('Error method {0} error: {1} code: {2}', [
        '{0}' => $method,
        '{1}' => $error_message,
        '{2}' => $error_code,
      ]);
      $logger->error($mss);
      throw new \Exception($mss);
    }
    return $resp_json;
  }
  /**
   * Validation Payment.
   *
   * @param array $data
   *   Form data.
   *
   * @return bool
   *   Validation result.
   *
   * @throws \Exception
   */
  private function validatePaymentForm(array $data) {
    $merchant_data       = $data;
    $validation_settings = Validation::factory($this->config)
                                     ->rule('storeId', 'not_empty')
                                     ->rule('password', 'not_empty');
    $validation_merchant = Validation::factory($merchant_data)
                                     ->rule('storeId', 'not_empty')
                                     ->rule('orderId', 'not_empty')
                                     ->rule('amount', 'not_empty')
                                     ->rule('partsCount', 'not_empty')
                                     ->rule('merchantType', 'not_empty')
                                     ->rule('products', 'not_empty')
                                     ->rule('responseUrl', 'not_empty')
                                     ->rule('redirectUrl', 'not_empty');
    if (!$validation_settings->check() or !$validation_merchant->check()) {
      throw new \Exception('Incorrect configuration' . '<br />' . implode('<br />',
          $validation_merchant->errors) . '<br />' . implode('<br />',
          $validation_settings->errors));
    }
    return TRUE;
  }
}