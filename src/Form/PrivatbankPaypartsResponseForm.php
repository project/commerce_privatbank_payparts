<?php

namespace Drupal\commerce_privatbank_payparts\Form;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_privatbank_payparts\Form\PrivatbankPaypartsFormTrait;
use Drupal\commerce_privatbank_payparts\Helpers\Arr;
use Drupal\commerce_privatbank_payparts\Helpers\Validation;

/**
 * Class PrivatbankPaypartsResponseForm.
 *
 * @package Drupal\commerce_privatbank_payparts\Form
 */
class PrivatbankPaypartsResponseForm {
  use PrivatbankPaypartsFormTrait;


  const STATUS_PAYMENT_AUTHORIZED = 'authorization';

  const STATUS_ORDER_COMPLETE = 'completed';

  const STATUS_PAYMENT_FAIL = 'authorization_voided';

  const STATUS_PAYMENT_REFUNDED = 'refunded';

  public $cleanedData = [];

  public $data = [];

  public $isDirty = FALSE;

  public $user;
  public $payment_gateway;
  public $payment_gateway_id;
  /**
   * Order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  public $order;

  public $errors = [];

  /**
   * Constructor.
   *
   * @param array $data
   *   Form data.
   * @param array $config
   *   Payment Gateway config.
   */
  public function __construct(array $data, array $config) {
    $this->data = $data;
    $this->config = $config;
  }

  /**
   * Check form values.
   *
   * @return bool
   *   Result.
   */
  public function isValid() {
    if (empty($this->data)) {
      return FALSE;
    }
    try {
      $this->clean();
    }
    catch (\Exception  $e) {
      $this->errors[] = $e->__toString();
      return FALSE;
    }
    $validation = Validation::factory($this->cleanedData)
      ->rule('storeId', 'not_empty')
      ->rule('orderId', 'not_empty')
      ->rule('paymentState', 'not_empty')
      ->rule('message', 'not_empty')
      ->rule('signature', 'not_empty');
    $is_valid = $validation->check();
    if ($is_valid === FALSE) {
      $this->errors = $validation->errors;
    }
    return $is_valid;
  }

  /**
   * Perform validation.
   *
   * @return array
   *   Cleaned data.
   *
   * @throws \Exception
   */
  public function clean() {
    /** @var \Drupal\Core\Logger\LoggerChannelInterface $logger */
    $logger                             = \Drupal::logger('commerce_privatbank_payparts');
    $this->cleanedData                  = Arr::extract($this->data, [
      'storeId',
      'orderId',
      'paymentState',
      'message',
      'signature',
    ]);
    $cleaned_data                       = $this->cleanedData;
    $cleaned_data['storeId']  = $this->config['storeId'];
    $order_id                           = $cleaned_data['orderId'];
    $this->order                        = Order::load(explode('-',$order_id)[0]);
    if (!$this->order) {
      $logger->error(
        "PrivatbankPaypartsPaymentResponseForm invalid order {$order_id}  does not exists");
      throw new \Exception("PrivatbankPaypartsPaymentResponseForm invalid order {$order_id}  does not exists");
    }
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order_entity */
    $request_signature = $cleaned_data['signature'];
    $sign_data = [];

    foreach ([
      'storeId',
      'orderId',
      'paymentState',
      'message',
    ] as $field_name) {
      if (isset($cleaned_data[$field_name])) {
        $sign_data[$field_name] = $cleaned_data[$field_name];
      }
    }
    $origin_signature = $this->makeSignature($sign_data);
    if ($request_signature !== $origin_signature) {
      $logger->error('PrivatbankPaypartsPaymentResponseForm invalid merchantSignature');
      throw new \Exception('PrivatbankPaypartsPaymentResponseForm invalid merchantSignature');
    }
    return $cleaned_data;
  }

  /**
   * Save form.
   */
  public function save() {
    $cleaned_data = $this->cleanedData;
    $transaction_status = $cleaned_data['paymentState'];
    /** @var \Drupal\commerce_payment\Entity\Payment $payment_storage */
    $payment_storage = \Drupal::service('entity_type.manager')
      ->getStorage('commerce_payment');
    if ($transaction_status == 'CLIENT_WAIT') {
    }
    elseif ($transaction_status == 'SUCCESS') {
      $payment = $payment_storage->create([
        'state' => self::STATUS_ORDER_COMPLETE,
        'amount' => $this->order->getTotalPrice(),
        'payment_gateway' => $this->payment_gateway,
        'order_id' => $this->order->id(),
        'remote_id' => $cleaned_data['orderId'],
        'remote_state' => $transaction_status,
      ]);
      $payment->save();
    }
    elseif (in_array($transaction_status, ['CANCELED'])) {
      $payment = $payment_storage->create([
        'state' => self::STATUS_PAYMENT_REFUNDED,
        'amount' => $this->order->getTotalPrice(),
        'payment_gateway' => $this->payment_gateway,
        'order_id' => $this->order->id(),
        'remote_id' => $cleaned_data['orderId'],
        'remote_state' => $transaction_status,
      ]);
      $payment->save();
    }
    elseif (in_array($transaction_status,
      ['FAIL'])) {
      $payment = $payment_storage->create([
        'state' => self::STATUS_PAYMENT_FAIL,
        'amount' => $this->order->getTotalPrice(),
        'payment_gateway' => $this->payment_gateway,
        'order_id' => $this->order->id(),
        'remote_id' => $cleaned_data['orderId'],
        'remote_state' => $transaction_status,
      ]);
      $payment->save();
    }
  }

}
